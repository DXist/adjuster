# coding: utf-8

"""Tests for adjuster module."""

from __future__ import print_function
from __future__ import unicode_literals
import logging
import unittest

import adjuster

logging.basicConfig(level=logging.DEBUG, format=adjuster.MULTILINE_FORMAT)


def string_reader(text):
    """Read lines from string."""
    for l in text.splitlines(True):
        yield l


class JustifyTestCase(unittest.TestCase):

    """Justifier tests."""

    def _justify_and_check(self, text, width, num_of_paragraphs):
        nonlocals = {'num_of_paragraphs': 0, 'line_lengths': []}

        def len_check_writer():
            while True:
                paragraph = (yield)
                lines = paragraph.splitlines()

                # append lengths of paragraph lines except last one and one
                # word lines
                nonlocals['line_lengths'].append(
                    [len(line) for line in lines[:-1]
                     if adjuster.num_of_words(line) > 1])
                nonlocals['num_of_paragraphs'] += 1

        reader = string_reader(text)
        writer = len_check_writer()
        adjuster.justify(reader, writer, width)
        self.assertEqual(nonlocals['num_of_paragraphs'], num_of_paragraphs)
        for lengths in nonlocals['line_lengths']:
            for length in lengths:
                self.assertEqual(length, width)

    def test_ascii(self):
        """Test ascii text."""

        self._justify_and_check(ascii_text, 30, 3)

    def test_sample_text(self):
        """Test sample from real text."""
        self._justify_and_check(unicode_text, 30, 8)

    def test_wide_justification(self):
        """Test of very wide justification width."""
        self._justify_and_check(ascii_text, 120, 3)

    def test_narrow_justification(self):
        """Test of very narrow justification width."""
        self._justify_and_check(unicode_text, 20, 8)


ascii_text = """
   Lorem ipsum

Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
cillum dolore eu fugiat nulla pariatur.



Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
cillum dolore eu fugiat nulla pariatur."""


unicode_text = """
 Джениферу Брехлу, "Зеленому карандашу", самому
 лучшему и работоспособному редактору в мире...


 МАТЕМАТИК

 КЛЕОН I -... Последний Галактический Император
 из династии Интан.Год рождения - 11988
 галактической эры; в этом же году родился Хари
 Селдон. (Полагают, что дата рождения Селдона,
 которую многие считают сомнительной, - могла
 быть намеренно подогнана к дате рождения
 Клеона, с которым Селдон, вскоре после
 прибытия на Трантор, неожиданно столкнулся).
 Клеон I унаследовал трон в 12010 году, в
 возрасте двадцати двух лет. Период его
 царствования ознаменовался удивительным, в те
 тревожные времена, покоем.
 Несомненно, заслуга в этом принадлежит Главе
 Штаба - Это Демерзелу, который так умело
 оставался в тени, что о нем почти ничего не
 известно. Сам Клеон...
 Галактическая энциклопедия.*
 * Все цитаты, приводимые из Галактической энциклопедии, взяты из 116-ого
 издания, опубликованного в 1020 г. Э.О., с разрешения издателей.

 1.
 Сдерживая легкий зевок, Клеон поинтересовался:
 - Демерзел, тебе приходилось когда-нибудь слышать о человеке по имени Хари
 Селдон? Клеон императорствовал уже более десяти лет и иногда - во время торжественных церемоний, когда он облачался в парадные одежды и регалии, - напускал на себя величественный вид. 	Именно так он выглядел на голографии, установленной в стенной нише за его спиной. Портрет размещался таким образом, что занимал достойное место в галерее его родовитых предков, в других нишах. Однако, это был явно парадный портрет - на изображении светло-каштановые волосы были значительно гуще. Оно не отражало и легкой асимметрии лица - в жизни левый уголок его губ был чуть-чуть приподнят. А если бы живой Император встал за голограммой, то оказался бы на два сантиметра ниже 1,83-метрового роста портрета - и, возможно, несколько плотнее. Несомненно, голограмма представляла официальный портрет при коронации. Тогда он был моложе. Он и сейчас выглядел достаточно молодо и привлекательно; и когда не был скован безжалостной хваткой официальной церемонии, его лицо несло на себе легкий отпечаток добродушия. 	Демерзел ответил почтительно, он всегда подчеркнуто придерживался такого тона.
        """
