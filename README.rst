Text justifier
==============


Sample usage::

    $ ./adjuster.py --width 40 infile.txt outfile.txt


Run tests (tested on Python 2.7 and Python 3.4)::

    $ python -m unittest discover
