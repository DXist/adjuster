#!/usr/bin/env python
# coding: utf-8

"""Unicode text justifier."""
from __future__ import print_function
from __future__ import unicode_literals
import argparse
import errno
import io
import logging
import re
import sys
import textwrap

logger = logging.getLogger(__name__)

PARAGRAPH_END_CHARS = ('.', '?', '!', ':')
PARAGRAPH_INDENT = '   '
WRAPPER_PARAMS = {'initial_indent': PARAGRAPH_INDENT}
BROKEN_PIPE = 1
MULTILINE_FORMAT = ('%(levelname)s:%(name)s:%(message)s\n{}'
                    .format('0---------' * 12))


def justify(reader, writer, width):
    """Justify text to given width.

    :param reader: line text generator
    :param writer: paragraph output consumer generator
    :param width: text wrapping width

    """
    assert width >= 20
    writer.send(None)
    wrapper = textwrap.TextWrapper(width=width, **WRAPPER_PARAMS)
    paragraph_lines = []

    for line in reader:
        line = line.strip()
        # remove multiple whitespaces
        line = re.sub(r'\s+', ' ', line)
        if line:
            line_is_empty = False
            paragraph_lines.append(line)
        else:
            line_is_empty = True

        if line_is_empty and not paragraph_lines:
            logger.debug('Skip empty line before paragraph')
            continue

        # paragraph ends on special chars or delimited by empty line
        if line.endswith(PARAGRAPH_END_CHARS) or line_is_empty:
            paragraph = '\n'.join(paragraph_lines)
            logger.debug('Found paragraph:\n%s', paragraph)
            justified_paragraph = justify_paragraph(wrapper, paragraph)
            logger.debug('Justified paragraph:\n%s', justified_paragraph)
            status = writer.send(justified_paragraph)

            if status:
                # non empty status breaks reading
                logger.debug('Reading is stopped')
                break
            # start new paragraph
            paragraph_lines = []

    writer.close()


def justify_paragraph(wrapper, paragraph):
    """Justify paragraph to given width.

    :param wrapper: :class:`textwrap.TextWrapper` instance
    :param paragraph: paragraph string
    :returns: justified paragraph string

    """

    lines = wrapper.wrap(paragraph)
    width = wrapper.width
    # pad paragraph lines except last one
    padded_lines = [pad(line, width) for line in lines[:-1]]
    # last line goes as is
    padded_lines.append(lines[-1])
    justified_paragraph = '\n'.join(padded_lines) + '\n'
    return justified_paragraph


def pad(line, width):
    """Pad line words with spaces to justify to given width.

    :returns: padded line

    """
    padded_line = line
    if num_of_words(line) == 1:
        # there is only one word - nothing to justify
        logger.debug('Skip one word line')
        return padded_line

    head, sep, tail = line.partition(PARAGRAPH_INDENT)

    if not head and sep:
        logger.debug('Line is indented')
        topad = tail
    else:
        topad = line

    # we are going to pad part of incoming string
    current_width = len(topad)
    target_width = width - len(sep)

    # reverse line to replace spaces from the left
    reversed_line = topad[::-1]

    # start with 2 space word padding
    pad_width = 2

    while current_width < target_width:
        replace_count = target_width - current_width
        old_padding = ' ' * (pad_width - 1)
        new_padding = ' ' * pad_width
        reversed_line = reversed_line.replace(old_padding, new_padding,
                                              replace_count)
        new_width = len(reversed_line)

        if new_width == current_width:
            raise RuntimeError('Unexpected error for line {}'.format(line))

        current_width = new_width
        pad_width += 1

    padded_line = sep + reversed_line[::-1]
    return padded_line


def num_of_words(line):
    """Return number of words in line."""
    return len(line.split())


def ioreader(ioobj):
    """Read lines from io object."""
    for line in ioobj:
        yield line


def iowriter(ioobj):
    """Write chunks to io object."""

    try:
        while True:
            ioobj.write((yield))
    except IOError as e:
        if e.errno == errno.EPIPE:
            logger.debug('Broken pipe')
            yield BROKEN_PIPE
        else:
            raise
    finally:
        try:
            ioobj.close()
        except IOError as e:
            if e.errno == errno.EPIPE:
                pass
            else:
                raise


class FileType(object):

    """Factory for creating file object types.

    Taken from Python 3.4.

    Instances of FileType are typically passed as type= arguments to the
    ArgumentParser add_argument() method.

    Keyword Arguments:
        - mode -- A string indicating how the file is to be opened. Accepts the
            same values as the builtin open() function.
        - bufsize -- The file's desired buffer size. Accepts the same values as
            the builtin open() function.
        - encoding -- The file's encoding. Accepts the same values as the
            builtin open() function.
        - errors -- A string indicating how encoding and decoding errors are to
            be handled. Accepts the same value as io.open() function.

    """

    def __init__(self, mode='r', bufsize=-1, encoding=None, errors=None):
        self._mode = mode
        self._bufsize = bufsize
        self._encoding = encoding
        self._errors = errors

    def __call__(self, string):
        # the special argument "-" means sys.std{in,out}
        if string == '-':
            if 'r' in self._mode:
                return sys.stdin
            elif 'w' in self._mode:
                return sys.stdout
            else:
                msg = 'argument "-" with mode %r' % self._mode
                raise ValueError(msg)

        # all other arguments are used as file names
        try:
            return io.open(string, self._mode, self._bufsize, self._encoding,
                           self._errors)
        except IOError as e:
            message = "can't open '%s': %s"
            raise argparse.ArgumentError(message % (string, e))

    def __repr__(self):
        args = self._mode, self._bufsize
        kwargs = [('encoding', self._encoding), ('errors', self._errors)]
        args_str = ', '.join([repr(arg) for arg in args if arg != -1] +
                             ['%s=%r' % (kw, arg) for kw, arg in kwargs
                              if arg is not None])
        return '%s(%s)' % (type(self).__name__, args_str)


if sys.version_info > (3, 4):
    FileType = argparse.FileType  # noqa


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('infile', help='file name of text to justify',
                        type=FileType('r', encoding='utf-8'))
    parser.add_argument('outfile', help='output file name', nargs='?',
                        type=FileType('w', encoding='utf-8'),
                        default=sys.stdout)
    parser.add_argument('-w', '--width', help='justify width', type=int,
                        default=80)
    parser.add_argument('-v', '--verbose', help='verbose output',
                        action='store_true')
    arguments = parser.parse_args()

    if arguments.verbose:
        logging.basicConfig(level=logging.DEBUG, format=MULTILINE_FORMAT)

    r = ioreader(arguments.infile)
    w = iowriter(arguments.outfile)
    justify(r, w, arguments.width)
